import { Misc } from "./misc.js";

/* -------------------------------------------- */
export class RdDItemCompetenceCreature extends Item {

  /* -------------------------------------------- */
  static setRollDataCreature(rollData) {
    rollData.competence = Misc.data(rollData.competence);
    rollData.carac = { "carac_creature": { label: rollData.competence.name, value: rollData.competence.data.carac_value } };
    rollData.competence.data.defaut_carac = "carac_creature";
    rollData.competence.data.categorie = "creature";
    rollData.selectedCarac = rollData.carac.carac_creature;
    if (rollData.competence.data.iscombat) {
      rollData.arme = RdDItemCompetenceCreature.toArme(rollData.competence);
    }

  }

  /* -------------------------------------------- */
  static toArme(itemData) {
    if (RdDItemCompetenceCreature.isCompetenceAttaque(itemData)) {
      itemData = Misc.data(itemData);
      let arme = { name: itemData.name, data: duplicate(itemData) };
      mergeObject(arme.data,
        {
          competence: itemData.name,
          resistance: 100,
          equipe: true,
          dommagesReels: arme.data.dommages,
          penetration: 0,
          force: 0,
          rapide: true
        });
      return arme;
    }
    console.error("RdDItemCompetenceCreature.toArme(", itemData, ") : impossible de transformer l'Item en arme");
    return undefined;
  }

  /* -------------------------------------------- */
  static isCompetenceAttaque(itemData) {
    itemData = Misc.data(itemData);
    return itemData.type == 'competencecreature' && itemData.data.iscombat;
  }
  
  /* -------------------------------------------- */
  static isCompetenceParade(itemData) {
    itemData = Misc.data(itemData);
    return itemData.type == 'competencecreature' && itemData.data.isparade;
  }
}  
