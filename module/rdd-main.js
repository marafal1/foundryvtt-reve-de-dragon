/**
 * RdD system
 * Author: LeRatierBretonnien
 * Software License: GNU GPLv3
 */

/* -------------------------------------------- */

/* -------------------------------------------- */
// Import Modules
import { RdDActor } from "./actor.js";
import { RdDItemSheet } from "./item-sheet.js";
import { RdDActorSheet } from "./actor-sheet.js";
import { RdDActorCreatureSheet } from "./actor-creature-sheet.js";
import { RdDActorVehiculeSheet } from "./actor-vehicule-sheet.js";
import { RdDActorEntiteSheet } from "./actor-entite-sheet.js";
import { RdDUtility } from "./rdd-utility.js";
import { TMRUtility } from "./tmr-utility.js";
import { RdDCalendrier } from "./rdd-calendrier.js";
import { RdDResolutionTable } from "./rdd-resolution-table.js";
import { RdDTokenHud } from "./rdd-token-hud.js";
import { RdDCommands } from "./rdd-commands.js";
import { RdDCombatManager, RdDCombat } from "./rdd-combat.js";
import { ChatUtility } from "./chat-utility.js";
import { StatusEffects } from "./status-effects.js";
import { RddCompendiumOrganiser } from "./rdd-compendium-organiser.js";
import { ReglesOptionelles } from "./regles-optionelles.js";
import { TMRRencontres } from "./tmr-rencontres.js";
import { RdDHotbar } from "./rdd-hotbar-drop.js"
import { EffetsDraconiques } from "./tmr/effets-draconiques.js";
import { RdDHerbes } from "./rdd-herbes.js";
import { RdDItem } from "./item.js";
import { RdDDice } from "./rdd-dice.js";
import { RdDSigneDraconiqueItemSheet } from "./item-signedraconique-sheet.js";

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */



/************************************************************************************/
Hooks.once("init", async function () {
  console.log(`Initializing Reve de Dragon System`);

  // preload handlebars templates
  RdDUtility.preloadHandlebarsTemplates();
  // Create useful storage space
  game.system.rdd = {
    TMRUtility,
    RdDUtility,
    RdDHotbar
  }

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "accorder-entite-cauchemar", {
    name: "Accorder le rêve aux entités",
    hint: "A quel moment les personnages doivent accorder leur rêve aux entités de cauchemar",
    scope: "world",
    config: true,
    type: String,
    choices: {           // If choices are defined, the resulting setting will be a select menu
      "avant-attaque": "Avant l'attaque",
      "avant-defense": "Avant la défense",
      "avant-encaissement": "Avant l'encaissement",
    },
    default: "avant-encaissement"
  });

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "calendrier", {
    name: "calendrier",
    scope: "world",
    config: false,
    default: RdDCalendrier.getCalendrier(0),
    type: Object
  });

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "liste-nombre-astral", {
    name: "liste-nombre-astral",
    scope: "world",
    config: false,
    default: [],
    type: Object
  });

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "calendrier-pos", {
    name: "calendrierPos",
    scope: "client",
    config: false,
    default: RdDCalendrier.createCalendrierPos(),
    type: Object
  });

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "dice-so-nice", {
    name: "Montrer les dés pour toutes les jets",
    hint: "Utilise Dice So Nice pour tous les jets de dés possibles. Décocher pour limiter à la table de résolution",
    scope: "client",
    config: true,
    default: false,
    type: Boolean
  });

  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "supprimer-dialogues-combat-chat", {
    name: "Supprimer les dialogues de combat",
    hint: "Si désactivée, tous les dialogues de combat sont conservés dans la conversation",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });
  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "activer-sons-audio", {
    name: "Activer les bruitages intégrés",
    hint: "Si activé, certaines actions en jeu déclenchent un son d'ambiance",
    scope: "world",
    config: true,
    default: true,
    type: Boolean
  });
  /* -------------------------------------------- */
  game.settings.register("foundryvtt-reve-de-dragon", "appliquer-famine-soif", {
    name: "Notifier de la famine et la soif pour",
    hint: "Indique si les cas de famine et de soif seront indiqués durant Château Dormant",
    scope: "world",
    config: true,
    type: String,
    choices: {
      "aucun": "ni la famine, ni la soif",
      "famine": "seulement la famine",
      "famine-soif": "la famine et la soif",
    },
    default: "aucun"
  });

  /* -------------------------------------------- */
  // Set an initiative formula for the system 
  CONFIG.Combat.initiative = {
    formula: "1+(1d6/10)",
    decimals: 2
  };

  /* -------------------------------------------- */
  game.socket.on("system.foundryvtt-reve-de-dragon", sockmsg => {
    console.log(">>>>> MSG RECV", sockmsg);

    RdDUtility.onSocketMesssage(sockmsg);
    RdDCombat.onSocketMessage(sockmsg);
    ChatUtility.onSocketMessage(sockmsg);
    RdDActor.onSocketMessage(sockmsg);
  });

  /* -------------------------------------------- */
  // Define custom Entity classes
  CONFIG.Actor.documentClass = RdDActor;
  CONFIG.Item.documentClass = RdDItem;
  CONFIG.RDD = {
    resolutionTable: RdDResolutionTable.resolutionTable,
    carac_array: RdDUtility.getCaracArray(),
    ajustementsConditions: RdDUtility.getAjustementsConditions(),
    difficultesLibres: RdDUtility.getDifficultesLibres()
  }

  /* -------------------------------------------- */
  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("foundryvtt-reve-de-dragon", RdDActorSheet, { types: ["personnage"], makeDefault: true });
  Actors.registerSheet("foundryvtt-reve-de-dragon", RdDActorCreatureSheet, { types: ["creature"], makeDefault: true });
  Actors.registerSheet("foundryvtt-reve-de-dragon", RdDActorVehiculeSheet, { types: ["vehicule"], makeDefault: true });
  Actors.registerSheet("foundryvtt-reve-de-dragon", RdDActorEntiteSheet, { types: ["entite"], makeDefault: true });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("foundryvtt-reve-de-dragon", RdDSigneDraconiqueItemSheet, {
    label: "Signe draconique",
    types: ["signedraconique"],
    makeDefault: true
  });
  Items.registerSheet("foundryvtt-reve-de-dragon", RdDItemSheet, { makeDefault: true });
  CONFIG.Combat.documentClass = RdDCombatManager;

  // préparation des différents modules
  ReglesOptionelles.init();
  RdDUtility.init();
  RdDDice.init();
  RdDCommands.init();
  RdDCombat.init();
  RdDCombatManager.init();
  RdDTokenHud.init();
  RdDActor.init();
  RddCompendiumOrganiser.init();
  EffetsDraconiques.init()
  TMRUtility.init();
  TMRRencontres.init();
  RdDHotbar.initDropbar();
});

/* -------------------------------------------- */
function messageDeBienvenue() {
  if (game.user.isGM) {
    ChatUtility.removeChatMessageContaining('<div id="message-bienvenue-rdd">');
    ChatMessage.create({
      user: game.user.id,
      content: `<div id="message-bienvenue-rdd"><span class="rdd-roll-part">Bienvenue dans le Rêve des Dragons !</span>
      <br>Vous trouverez quelques informations pour démarrer dans ce document : @Compendium[foundryvtt-reve-de-dragon.rappel-des-regles.7uGrUHGdPu0EmIu2]{Documentation MJ/Joueurs}
      <br>La commande <code>/aide</code> dans le chat permet de voir les commandes spécifiques à Rêve de Dragon.</div>
      ` });
  }
}

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once("ready", async function () {

  StatusEffects.onReady();
  RdDHerbes.initializeHerbes();
  RdDDice.onReady();
  /* -------------------------------------------- */
  /* Affiche/Init le calendrier */
  let calendrier = new RdDCalendrier();
  await calendrier.initCalendrier();
  let templatePath = "systems/foundryvtt-reve-de-dragon/templates/calendar-template.html";
  let templateData = {};
  renderTemplate(templatePath, templateData).then(html => {
    calendrier.render(true);
  });
  game.system.rdd.calendrier = calendrier; // Reference;

  // Avertissement si joueur sans personnage
  if (!game.user.isGM && game.user.character == undefined) {
    ui.notifications.info("Attention ! Vous n'êtes connecté à aucun personnage !");
    ChatMessage.create({
      content: "<b>ATTENTION</b> Le joueur " + game.user.name + " n'est connecté à aucun personnage !",
      user: game.user.id
    });
    //whisper:  [ ChatMessage.getWhisperRecipients("GM") ] } );
  }

  messageDeBienvenue();
});

/* -------------------------------------------- */
/*  Dice-so-nice ready                          */
/* -------------------------------------------- */
Hooks.once('diceSoNiceReady', (dice3d) => RdDDice.diceSoNiceReady(dice3d));

/* -------------------------------------------- */
/*  Foundry VTT chat message                    */
/* -------------------------------------------- */
Hooks.on("chatMessage", (html, content, msg) => {
  if (content[0] == '/') {
    let regExp = /(\S+)/g;
    let commands = content.toLowerCase().match(regExp);
    if (game.system.rdd.commands.processChatCommand(commands, content, msg)) {
      return false;
    }
  }
  return true;
});

