import { ChatUtility } from "./chat-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { RdDItemSigneDraconique } from "./item-signedraconique.js";
import { Misc } from "./misc.js";
import { RdDRollTables } from "./rdd-rolltables.js";
import { TMRType, TMRUtility } from "./tmr-utility.js";

export class DialogCreateSigneDraconique extends Dialog {

  static async createSigneForActors() {
    const signe = await RdDItemSigneDraconique.randomSigneDraconique({ephemere: true});
    let dialogData = {
      signe: signe,
      tmrs: TMRUtility.listSelectedTMR(signe.data.typesTMR ?? []),
      actors: game.actors.filter(actor => actor.isHautRevant()).map(actor => {
        let actorData = duplicate(Misc.data(actor));
        actorData.selected = actor.hasPlayerOwner;
        return actorData;
      })
    };

    const html = await renderTemplate("systems/foundryvtt-reve-de-dragon/templates/dialog-create-signedraconique.html", dialogData);
    new DialogCreateSigneDraconique(dialogData, html)
      .render(true);
  }

  constructor(dialogData, html, callback) {
    let options = { classes: ["DialogCreateSigneDraconiqueActorsActors"], width: 500, height: 650, 'z-index': 99999 };
    let conf = {
      title: "Créer un signe",
      content: html,
      default: "Ajouter aux haut-rêvants",
      buttons: {
        "Ajouter aux haut-rêvants": { label: "Ajouter aux haut-rêvants", callback: it => { this._onCreerSigneActeurs(); } }
      }
    };
    super(conf, options);
    this.dialogData = dialogData;
  }

  async _onCreerSigneActeurs() {
    await $("[name='signe.data.ephemere']").change();
    await $(".signe-xp-sort").change();
    this.validerSigne();
    this.dialogData.actors.filter(it => it.selected).map(it => game.actors.get(it._id))
      .forEach(actor => this._createSigneForActor(actor, this.dialogData.signe));
  }

  async _createSigneForActor(actor, signe) {
    actor.createEmbeddedDocuments("Item", [signe]);
    ChatMessage.create({
      whisper: ChatUtility.getWhisperRecipientsAndGMs(Misc.data(actor).name),
      content: await renderTemplate("systems/foundryvtt-reve-de-dragon/templates/chat-signe-draconique-actor.html", {
        signe: signe,
        alias: Misc.data(actor).name
      })
    });
  }

  validerSigne() {
    this.dialogData.signe.name = $("[name='signe.name']").val();
    this.dialogData.signe.data.valeur.norm = $("[name='signe.data.valeur.norm']").val();
    this.dialogData.signe.data.valeur.sign = $("[name='signe.data.valeur.sign']").val();
    this.dialogData.signe.data.valeur.part = $("[name='signe.data.valeur.part']").val();
    this.dialogData.signe.data.difficulte = $("[name='signe.data.difficulte']").val();
    this.dialogData.signe.data.ephemere = $("[name='signe.data.ephemere']").prop("checked");
    this.dialogData.signe.data.duree = $("[name='signe.data.duree']").val();
    this.dialogData.signe.data.typesTMR = $(".select-tmr").val();
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);
    this.setEphemere(this.dialogData.signe.data.ephemere);
    html.find(".signe-aleatoire").click(event => this.setSigneAleatoire());
    html.find("[name='signe.data.ephemere']").change((event) => this.setEphemere(event.currentTarget.checked));
    html.find(".select-actor").change((event) => this.onSelectActor(event));
    html.find(".signe-xp-sort").change((event) => this.onValeurXpSort(event));
  }

  async setSigneAleatoire() {
    const newSigne = await RdDItemSigneDraconique.randomSigneDraconique({ephemere: true});

    $("[name='signe.name']").val(newSigne.name);
    $("[name='signe.data.valeur.norm']").val(newSigne.data.valeur.norm);
    $("[name='signe.data.valeur.sign']").val(newSigne.data.valeur.sign);
    $("[name='signe.data.valeur.part']").val(newSigne.data.valeur.part);
    $("[name='signe.data.difficulte']").val(newSigne.data.difficulte);
    $("[name='signe.data.duree']").val(newSigne.data.duree);
    $("[name='signe.data.ephemere']").prop("checked", newSigne.data.ephemere);
    $(".select-tmr").val(newSigne.data.typesTMR);
    this.setEphemere(newSigne.data.ephemere);
  }

  async setEphemere(ephemere) {
    this.dialogData.signe.data.ephemere = ephemere;
    HtmlUtility._showControlWhen($(".signe-data-duree"), ephemere);
  }

  async onSelectActor(event) {
    event.preventDefault();
    const options = event.currentTarget.options;
    for (var i = 0; i < options.length; i++) { // looping over the options
      const actorId = options[i].attributes["data-actor-id"].value;
      const actor = this.dialogData.actors.find(it => it._id == actorId);
      if (actor) {
        actor.selected = options[i].selected;
      }
    };
  }

  onValeurXpSort(event) {
    const codeReussite = event.currentTarget.attributes['data-typereussite']?.value ?? 0;
    const xp = Number(event.currentTarget.value);
    const oldValeur = this.dialogData.signe.data.valeur;
    this.dialogData.signe.data.valeur = RdDItemSigneDraconique.calculValeursXpSort(codeReussite, xp, oldValeur);
  }

}