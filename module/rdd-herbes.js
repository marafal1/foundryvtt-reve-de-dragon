/* -------------------------------------------- */
import { RdDUtility } from "./rdd-utility.js";
import { Misc } from "./misc.js";
import { RdDCalendrier } from "./rdd-calendrier.js";

/* -------------------------------------------- */
export class RdDHerbes extends Item {

  /* -------------------------------------------- */
  static isHerbeSoin( botaniqueItem ) {
    return Misc.templateData(botaniqueItem).categorie == 'Soin';
  }
  /* -------------------------------------------- */
  static isHerbeRepos( botaniqueItem ) {
    return Misc.templateData(botaniqueItem).categorie == 'Repos';
  }

  /* -------------------------------------------- */
  static async initializeHerbes( ) {  
    this.herbesSoins = await RdDUtility.loadCompendium('foundryvtt-reve-de-dragon.botanique', item => this.isHerbeSoin(item));
    this.herbesRepos = await RdDUtility.loadCompendium('foundryvtt-reve-de-dragon.botanique', item => this.isHerbeRepos(item));
  }

  /* -------------------------------------------- */
  static buildHerbesList(listHerbes, max) {
    let list = {}
    for ( let herbe of listHerbes) {
      let herbeData = Misc.templateData(herbe);
      let brins = max - herbeData.niveau;
      list[herbe.data.name] = `${herbe.data.name} (Bonus: ${herbeData.niveau}, Brins: ${brins})`;
    }
    list['Autre'] = 'Autre (Bonus: variable, Brins: variable)'
    return list;
  }

  /* -------------------------------------------- */
  static updatePotionData( formData ) {
    formData.herbesSoins = this.buildHerbesList(this.herbesSoins, 12);
    formData.herbesRepos = this.buildHerbesList(this.herbesRepos, 7);
    formData.jourMoisOptions = Array(28).fill().map((item, index) => 1 + index);
    formData.dateActuelle = game.system.rdd.calendrier.getDateFromIndex();
    formData.splitDate = game.system.rdd.calendrier.getNumericDateFromIndex(formData.data.prdate);

    if (formData.data.categorie.includes('Soin') ) {
      formData.isHerbe = true;
      this.computeHerbeBonus(formData, this.herbesSoins, 12);
    } else if (formData.data.categorie.includes('Repos')) {
      formData.isRepos = true;
      this.computeHerbeBonus(formData, this.herbesRepos, 7);
    }
  }

  /* -------------------------------------------- */
  static calculePointsRepos( data ) {
    return data.herbebonus * data.pr;
  }

  /* -------------------------------------------- */
  static calculePointsGuerison( data ){
    return data.herbebonus * data.pr;
  }

  /* -------------------------------------------- */
  static computeHerbeBonus( formData, herbesList, max) {
    if ( Number(formData.data.herbebrins) ) {
    let herbe = herbesList.find(item => item.name.toLowerCase() == formData.data.herbe.toLowerCase() );
      if( herbe ) {
        let herbeData = Misc.templateData(herbe);
        let brinsBase = max - herbeData.niveau;
        //console.log(herbeData, brinsBase, formData.data.herbebrins);
        formData.data.herbebonus = Math.max(herbeData.niveau - Math.max(brinsBase - formData.data.herbebrins, 0), 0);
      }
    }
  }  

}  