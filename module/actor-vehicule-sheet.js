/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { Misc } from "./misc.js";

/* -------------------------------------------- */  
export class RdDActorVehiculeSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
    RdDUtility.initAfficheContenu();

	  return mergeObject(super.defaultOptions, {
  	  classes: ["rdd", "sheet", "actor"],
  	  template: "systems/foundryvtt-reve-de-dragon/templates/actor-vehicule-sheet.html",
      width: 640,
      height: 720,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac"}],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */  
  _checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }

  /* -------------------------------------------- */
  async getData() {
    const objectData = Misc.data(this.object);
    let formData = {
      title: this.title,
      id: objectData.id,
      type: objectData.type,
      img: objectData.img,
      name: objectData.name,
      editable: this.isEditable,
      cssClass: this.isEditable ? "editable" : "locked",
      data: foundry.utils.deepClone(Misc.templateData(this.object)),
      effects: this.object.effects.map(e => foundry.utils.deepClone(e.data)),
      limited: this.object.limited,
      options: this.options,
      owner: this.document.isOwner,
      itemsByType: Misc.classify(this.object.items.map(i => foundry.utils.deepClone(i.data))),
    };

    RdDUtility.filterItemsPerTypeForSheet(formData);
    this.objetVersConteneur = RdDUtility.buildArbreDeConteneurs(formData.conteneurs, formData.objets);
    formData.conteneurs = RdDUtility.conteneursRacine(formData.conteneurs);

    formData.options.isGM = game.user.isGM;

    formData.calc ={
      encTotal: await this.actor.computeEncombrementTotalEtMalusArmure(),
    }
    formData.calc.surEncombrementMessage = formData.calc.encTotal > formData.data.capacite_encombrement ? "Sur-Encombrement!" : "",

    console.log("DATA", formData);

    return formData;
  }
  
  /* -------------------------------------------- */
  async _onDropItem(event, dragData) {
    const callSuper = await this.actor.processDropItem(event, dragData, this.objetVersConteneur);
    if (callSuper) {
      await super._onDropItem(event, dragData)
    }
  }

  /* -------------------------------------------- */
  async createItem(name, type) {
    await this.actor.createEmbeddedDocuments('Item', [{ name: name, type: type }], { renderSheet: true });
  }

  /* -------------------------------------------- */
  async monnaieIncDec(id, value) {
    let monnaie = this.getMonnaie(id);
    if (monnaie) {
      const quantite = Math.max(0, Misc.templateData(monnaie).quantite + value);
      await this.updateEmbeddedDocuments('Item', [{ _id: monnaie.id, 'data.quantite': quantite }]);
    }
  }

  /* -------------------------------------------- */
  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getEmbeddedDocument('Item', li.data("itemId"));
      item.sheet.render(true);
    });
    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      RdDUtility.confirmerSuppression(this, li);
    });

    html.find('.creer-un-objet').click(async event => {
      RdDUtility.selectObjetType( this );
    });
    html.find('#nettoyer-conteneurs').click(async event => {
      this.actor.nettoyerConteneurs();
    });

    html.find('.monnaie-plus').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), 1);
    });
    html.find('.monnaie-moins').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), -1);
    });

    // Display info about queue
    html.find('.conteneur-name a').click((event) => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      RdDUtility.toggleAfficheContenu(myID);
      this.render(true);
    });

  }
  
  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetHeader = this.element.find(".sheet-header");
    const sheetTabs = this.element.find(".sheet-tabs");
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - sheetHeader[0].clientHeight - sheetTabs[0].clientHeight;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
