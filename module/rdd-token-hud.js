/* -------------------------------------------- */
import { HtmlUtility } from "./html-utility.js";
import { Misc } from "./misc.js";
import { RdDCombatManager } from "./rdd-combat.js";

/* -------------------------------------------- */
export class RdDTokenHud {

  static init() {
    // Integration du TokenHUD
    Hooks.on('renderTokenHUD', (app, html, data) => { RdDTokenHud.addTokenHudExtensions(app, html, data._id) });
  }

  /* -------------------------------------------- */
  static async removeExtensionHud(app, html, tokenId) {
    html.find('.control-icon.rdd-combat').remove();
    html.find('.control-icon.rdd-initiative').remove();
  }

  /* -------------------------------------------- */
  static async addExtensionHud(app, html, tokenId) {

    let token = canvas.tokens.get(tokenId);
    let actor = token.actor;
    let combatant = game.combat.combatants.find(c => Misc.data(c).tokenId == tokenId);
    app.hasExtension = true;

    let armesList = RdDCombatManager.buildListeActionsCombat(combatant);
    const hudData = {
      combatant: combatant, armes: armesList,
      commandes: [{ name: 'Initiative +1', command: 'inc', value: 0.01 }, { name: 'Initiative -1', command: 'dec', value: -0.01 }]
    };

    const controlIconCombat = html.find('.control-icon[data-action=combat]');
    // initiative
    await RdDTokenHud._configureSubMenu(controlIconCombat, 'systems/foundryvtt-reve-de-dragon/templates/hud-actor-init.html', hudData,
      (event) => {
        let initCommand = event.currentTarget.attributes['data-command'].value;
        let combatantId = event.currentTarget.attributes['data-combatant-id'].value;
        if (!initCommand) {
          let armeIndex = event.currentTarget.attributes['data-arme-id'].value;
          let arme = armesList[armeIndex];
          RdDCombatManager.rollInitiativeCompetence(combatantId, arme);
        } else if (initCommand == 'inc') {
          RdDCombatManager.incDecInit(combatantId, 0.01);
        } else if (initCommand == 'dec') {
          RdDCombatManager.incDecInit(combatantId, -0.01);
        }
      });

    const controlIconTarget = html.find('.control-icon[data-action=target]');
    // combat
    await RdDTokenHud._configureSubMenu(controlIconTarget, 'systems/foundryvtt-reve-de-dragon/templates/hud-actor-attaque.html', hudData,
      (event) => {
        let armeIndex = event.currentTarget.attributes['data-arme-id'].value;
        let arme = armesList[armeIndex];
        actor.rollArme(arme.data.competence, arme.name);
      });
  }

  /* -------------------------------------------- */
  static async addTokenHudExtensions(app, html, tokenId) {
    const controlIconCombat  = html.find('.control-icon[data-action=combat]');
    controlIconCombat.click(event => {
      if (event.currentTarget.className.includes('active')) {
        RdDTokenHud.removeExtensionHud(app, html, tokenId);
      } else {
        setTimeout(function () { RdDTokenHud.addExtensionHud(app, html, tokenId) }, 200);
      }
    });

    if (controlIconCombat.length>0 && controlIconCombat[0].className.includes('active')) {
      RdDTokenHud.addExtensionHud(app, html, tokenId);
    }
  }

  /* -------------------------------------------- */
  static async _configureSubMenu(insertionPoint, template, hudData, onMenuItem) {
    const hud = $(await renderTemplate(template, hudData));
    const list = hud.find('div.rdd-hud-list');
    
    RdDTokenHud._toggleHudListActive(hud, list);
    
    hud.find('img.rdd-hud-togglebutton').click(event => RdDTokenHud._toggleHudListActive(hud, list));
    list.find('.rdd-hud-menu').click(onMenuItem);

    insertionPoint.after(hud);
  }

  static _toggleHudListActive(hud, list) {
    hud.toggleClass('active');
    HtmlUtility._showControlWhen(list, hud.hasClass('active'));
  }
}