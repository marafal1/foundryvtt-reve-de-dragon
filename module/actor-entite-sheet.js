/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { HtmlUtility } from "./html-utility.js";
import { Misc } from "./misc.js";
import { RdDUtility } from "./rdd-utility.js";

/* -------------------------------------------- */  
export class RdDActorEntiteSheet extends ActorSheet {

  /** @override */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["rdd", "sheet", "actor"],
  	  template: "systems/foundryvtt-reve-de-dragon/templates/actor-entite-sheet.html",
      width: 640,
      height: 720,
      tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac"}],
      dragDrop: [{dragSelector: ".item-list .item", dropSelector: null}]
    });
  }

  /* -------------------------------------------- */  
  _checkNull(items) {
    if (items && items.length) {
      return items;
    }
    return [];
  }

  /* -------------------------------------------- */
  async getData() {
    const objectData = Misc.data(this.object);
    let formData = {
      title: this.title,
      id: objectData.id,
      type: objectData.type,
      img: objectData.img,
      name: objectData.name,
      //   actor: this.object,
      editable: this.isEditable,
      cssClass: this.isEditable ? "editable" : "locked",
      data: foundry.utils.deepClone(Misc.templateData(this.object)),
      effects: this.object.effects.map(e => foundry.utils.deepClone(e.data)),
      //   items: items,
      limited: this.object.limited,
      options: this.options,
      owner: this.document.isOwner,
      itemsByType: Misc.classify(this.object.items.map(i => foundry.utils.deepClone(i.data))),
    };
    
    formData.options.isGM = game.user.isGM;
    RdDUtility.filterItemsPerTypeForSheet(formData);


    return formData;
  }
  
  /* -------------------------------------------- */
  /** @override */
	activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Update Inventory Item
    html.find('.item-edit').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      const item = this.actor.getEmbeddedDocument('Item', li.data("itemId"));
      item.sheet.render(true);
    });

    // Delete Inventory Item
    html.find('.item-delete').click(ev => {
      const li = $(ev.currentTarget).parents(".item");
      this.actor.deleteEmbeddedDocuments('Item', [li.data("itemId")]);
      li.slideUp(200, () => this.render(false));
    });

    // Roll Carac
    html.find('.carac-label a').click((event) => {
      let caracName = event.currentTarget.attributes.name.value;
      this.actor.rollCarac( caracName.toLowerCase() );
    });
    
    // On competence change
    html.find('.creature-carac').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "carac_value", parseInt(event.target.value) );
      } );    
    html.find('.creature-niveau').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "niveau", parseInt(event.target.value) );
      } );    
      html.find('.creature-dommages').change((event) => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCreatureCompetence( compName, "dommages", parseInt(event.target.value) );
      } );    
  
    // Roll Skill
    html.find('.competence-label a').click((event) => {
      let compName = event.currentTarget.text;
      this.actor.rollCompetence( compName );
    });

    html.find('#endurance-plus').click((event) => {
      this.actor.santeIncDec("endurance", 1);
      this.render(true);
    });
    html.find('#endurance-moins').click((event) => {
      this.actor.santeIncDec("endurance", -1);
      this.render(true);
    });

    html.find('.encaisser-direct').click(ev => {
      this.actor.encaisser();
    });

    html.find('.remise-a-neuf').click(ev => {
      if (game.user.isGM) {
        this.actor.remiseANeuf();
      }
    });
  }
  

  /* -------------------------------------------- */

  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetHeader = this.element.find(".sheet-header");
    const sheetTabs = this.element.find(".sheet-tabs");
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - sheetHeader[0].clientHeight - sheetTabs[0].clientHeight;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
