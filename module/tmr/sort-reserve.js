import { tmrColors, tmrConstants, tmrTokenZIndex } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class SortReserve extends Draconique {

  constructor() {
    super();
  }

  type() { return '' }
  match(item) { return false; }
  manualMessage() { return false }
  async onActorCreateOwned(actor, item) { }

  code() { return 'sort' }
  tooltip(sort) { return `${sort.name}, r${sort.data.ptreve_reel}` }
  img() { return 'systems/foundryvtt-reve-de-dragon/icons/tmr/scroll.webp' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(), {
      zIndex: tmrTokenZIndex.sort,
      alpha: 0.5,
      decallage: tmrConstants.right
    });
  }
}
