import { ChatUtility } from "../chat-utility.js";
import { Grammar } from "../grammar.js";
import { tmrConstants, tmrTokenZIndex, TMRUtility } from "../tmr-utility.js";
import { Draconique } from "./draconique.js";

export class PresentCites extends Draconique {

  constructor() {
    super();
  }

  type() { return 'tete' }
  match(item) { return Draconique.isTeteDragon(item) && Grammar.toLowerCaseNoAccent(item.name).includes('present des cites'); }
  manualMessage() { return false }
  async onActorCreateOwned(actor, tete) { await this._ajouterPresents(actor, tete); }

  code() { return 'present-cites' }
  tooltip(linkData) { return `La ${this.tmrLabel(linkData)} a un présent` }
  img() { return 'systems/foundryvtt-reve-de-dragon/icons/tmr/gift.webp' }

  createSprite(pixiTMR) {
    return pixiTMR.sprite(this.code(),
      {
        zIndex: tmrTokenZIndex.tetes,
        alpha: 0.9,
        taille: tmrConstants.third,
        decallage: tmrConstants.topRight
      });
  }

  async _ajouterPresents(actor, tete) {
    let existants = actor.data.items.filter(it => this.isCase(it)).map(it => it.data.coord);
    if (existants.length > 0) {
      ChatMessage.create({
        whisper: ChatUtility.getWhisperRecipientsAndGMs(game.user.name),
        content: "Vous avez encore des présents dans des cités, vous devrez tirer une autre tête pour remplacer celle ci!"
      })
    }
    else {
      let cites = TMRUtility.filterTMR(it => it.type == 'cite');
      for (let tmr of cites) {
        await this.createCaseTmr(actor, 'Présent: ' + tmr.label, tmr, tete.id);
      }
    }
  }

  async choisirUnPresent(casetmr, onChoixPresent) {
    let d = new Dialog({
      title: "Présent des cités",
      content: `La ${this.tmrLabel(casetmr)} vous offre un présent, faite votre choix`,
      buttons: {
        messager: { icon: '<i class="fas fa-check"></i>', label: "Un Messager des rêves", callback: () => onChoixPresent('messager2d6') },
        passeur: { icon: '<i class="fas fa-check"></i>', label: "Un Passeur des rêves", callback: () => onChoixPresent('passeur2d6') },
        fleur: { icon: '<i class="fas fa-check"></i>', label: "Une Fleur des rêves", callback: () => onChoixPresent('fleur2d6') },
      },
      default: "fleur"
    });
    d.render(true);
  }

  async ouvrirLePresent(actor, casetmr) {
    await actor.deleteEmbeddedDocuments('Item', [casetmr.id]);
  }
}
