export class RdDRollTables {

  /* -------------------------------------------- */
  static async genericGetTableResult(tableName, toChat) {
    const pack = game.packs.get("foundryvtt-reve-de-dragon.tables-diverses");
    const index = await pack.getIndex();
    const entry = index.find(e => e.name === tableName);
    const table = await pack.getDocument(entry._id);
    const draw = await table.draw({ displayChat: toChat, rollMode: "gmroll"});
    console.log("RdDRollTables", tableName, toChat, ":", draw);
    return draw.results.length > 0 ? draw.results[0] : undefined;
  }

  /* -------------------------------------------- */
  static async drawItemFromRollTable(tableName, toChat = false) {
    const drawResult = await RdDRollTables.genericGetTableResult(tableName, toChat);
    const pack = game.packs.get(drawResult.data.collection);
    return await pack.getDocument(drawResult.data.resultId);
  }
  
  /* -------------------------------------------- */
  static async drawTextFromRollTable(tableName, toChat) {
    const drawResult = await RdDRollTables.genericGetTableResult(tableName, toChat);
    return drawResult.data.text;
  }

  /* -------------------------------------------- */
  static async getCompetence(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Détermination aléatoire de compétence", toChat);
  }

  /* -------------------------------------------- */
  static async getSouffle(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Souffles de Dragon", toChat);
  }

  /* -------------------------------------------- */
  static async getQueue(toChat = false) {
    let queue = await RdDRollTables.drawItemFromRollTable("Queues de dragon", toChat);
    if (queue.name.toLowerCase().includes('lancinant') ) {
      return await RdDRollTables.getDesirLancinant(toChat);
    }
    if (queue.name.toLowerCase().includes('fixe') ) {
      return await RdDRollTables.getIdeeFixe(toChat);
    }
    return queue;
  }

  static async getDesirLancinant(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Désirs lancinants", toChat);
  }
  
  static async getIdeeFixe(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Idées fixes", toChat);
  }

  /* -------------------------------------------- */
  static async getTeteHR(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Têtes de Dragon pour haut-rêvants", toChat);
  }

  /* -------------------------------------------- */
  static async getTete(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Têtes de Dragon pour tous personnages", toChat);
  }

  /* -------------------------------------------- */
  static async getOmbre(toChat = false) {
    return await RdDRollTables.drawItemFromRollTable("Ombre de Thanatos", toChat);
  }

  /* -------------------------------------------- */
  static async getTarot(toChat = true) {
    return await RdDRollTables.drawItemFromRollTable("Tarot Draconique", toChat);
  }

  /* -------------------------------------------- */
  static async getMaladresse(options = {toChat: false, arme: false}) {
    return await RdDRollTables.drawTextFromRollTable(
      options.arme ? "Maladresse armé" : "Maladresses non armé",
      options.toChat);
  }
}
