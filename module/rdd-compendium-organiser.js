import { Misc } from "./misc.js";

export class RddCompendiumOrganiser {
  static init() {
    Hooks.on('renderCompendium', async (pack, html, data) => RddCompendiumOrganiser.onRenderCompendium(pack, html, data))
  }

  static async onRenderCompendium(compendium, html, data) {
    console.log('onRenderCompendium', compendium, html, data);
    const pack = compendium.collection
    if (pack.metadata.system === 'foundryvtt-reve-de-dragon') {
      html.find('.directory-item').each((i, element) => {
        RddCompendiumOrganiser.setEntityTypeName(pack, element);
      });
    }
  }

  static async setEntityTypeName(pack, element) {
    const label = Misc.getEntityTypeLabel(await pack.getDocument(element.dataset.documentId));
    RddCompendiumOrganiser.insertEntityType(element, label);
  }

  static insertEntityType(element, label) {
    if (label) {
      element.children[1].insertAdjacentHTML('afterbegin', `<label class="type-compendium">${label}: </label>`);
    }
  }

}