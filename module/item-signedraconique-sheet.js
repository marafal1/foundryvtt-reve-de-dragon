import { RdDItemSigneDraconique } from "./item-signedraconique.js";
import { Misc } from "./misc.js";
import { TMRType, TMRUtility } from "./tmr-utility.js";

/**
 * Item sheet pour signes draconiques
 * @extends {ItemSheet}
 */
export class RdDSigneDraconiqueItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["foundryvtt-reve-de-dragon", "sheet", "item"],
      template: "systems/foundryvtt-reve-de-dragon/templates/item-signedraconique-sheet.html",
      width: 550,
      height: 550
    });
  }

  /* -------------------------------------------- */
  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();
    buttons.unshift({ class: "post", icon: "fas fa-comment", onclick: ev => this.item.postItem() });
    return buttons;
  }

  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetHeader = this.element.find(".sheet-header");
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - sheetHeader[0].clientHeight;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  async getData() {
    const formData = duplicate(Misc.data(this.object));
    mergeObject(formData, {
      title: formData.name,
      isGM: game.user.isGM,
      owner: this.document.isOwner,
      isOwned: this.actor ? true : false,
      actorId: this.actor?.id,
      editable: this.isEditable,
      cssClass: this.isEditable ? "editable" : "locked",
    });
    formData.tmrs = TMRUtility.listSelectedTMR(formData.data.typesTMR ?? []);
    return formData;
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    if (!this.options.editable) return;

    html.find(".signe-aleatoire").click(event => this.setSigneAleatoire());
    html.find(".select-tmr").change((event) => this.onSelectTmr(event));
    html.find(".signe-xp-sort").change((event) => this.onValeurXpSort(event.currentTarget.attributes['data-typereussite']?.value, Number(event.currentTarget.value)));
  }

  async setSigneAleatoire() {
    const newSigne = await RdDItemSigneDraconique.randomSigneDraconique();
    this.object.update(newSigne);
  }

  async onSelectTmr(event) {
    event.preventDefault();
    const selectedTMR = $(".select-tmr").val();
    this.object.update({ 'data.typesTMR': selectedTMR });
  }

  async onValeurXpSort(event) {
    const codeReussite = event.currentTarget.attributes['data-typereussite']?.value ?? 0;
    const xp = Number(event.currentTarget.value);
    const oldValeur = Misc.templateData(this.object).valeur;
    const newValeur = RdDItemSigneDraconique.calculValeursXpSort(codeReussite, xp, oldValeur);
    await this.object.update({ 'data.valeur': newValeur });
  }

  /* -------------------------------------------- */
  get template() {
    return `systems/foundryvtt-reve-de-dragon/templates/item-signedraconique-sheet.html`;
  }

  get title() {
    return `Signe draconique: ${this.object.name}`;
  }
}
