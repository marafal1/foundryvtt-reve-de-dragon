
import { Misc } from "./misc.js";
import { RdDUtility } from "./rdd-utility.js";

export class DialogItemAchat extends Dialog {

  static async onButtonAcheter(event) {
    const buttonAcheter = event.currentTarget;
    if (!buttonAcheter.attributes['data-jsondata']?.value) {
      ui.notifications.warn("Impossible d'acheter: informations sur l'objet manquantes")
      return;
    }
    const chatMessageIdVente = RdDUtility.findChatMessageId(buttonAcheter);

    const vendeurId = buttonAcheter.attributes['data-vendeurId']?.value;
    const vendeur = vendeurId ? game.actors.get(vendeurId) : undefined;
    const acheteur = RdDUtility.getSelectedActor();

    if (!acheteur && !vendeur) {
      ui.notifications.info("Pas d'acheteur ni de vendeur, aucun changement");
      return;
    }

    let venteData = DialogItemAchat.prepareVenteData(buttonAcheter, vendeurId, vendeur, acheteur);
    const html = await renderTemplate(`systems/foundryvtt-reve-de-dragon/templates/dialog-item-achat.html`, venteData);
    const dialog = new DialogItemAchat(html, vendeur, acheteur, venteData, chatMessageIdVente);
    dialog.render(true);
  }

  constructor(html, vendeur, acheteur, venteData, chatMessageIdVente) {
    let options = { classes: ["dialogachat"], width: 400, height: 300, 'z-index': 99999 };

    const actionAchat = venteData.prixLot > 0 ? "Acheter" : "Prendre";
    let conf = {
      title: actionAchat,
      content: html,
      default: actionAchat,
      buttons: {
        [actionAchat]: { label: actionAchat, callback: it => { this.onAchat(); } },
        "decliner": { label: "Décliner", callback: it => { } }
      }
    };

    super(conf, options);

    this.vendeur = vendeur;
    this.acheteur = acheteur;
    this.chatMessageIdVente = chatMessageIdVente;
    this.venteData = venteData;
  }

  static prepareVenteData(buttonAcheter, vendeurId, vendeur, acheteur) {
    const jsondata = buttonAcheter.attributes['data-jsondata']?.value;
    const prixLot = buttonAcheter.attributes['data-prixLot']?.value ?? 0;
    let venteData = {
      item: JSON.parse(jsondata),
      vendeurId: vendeurId,
      vendeur: Misc.data(vendeur),
      acheteur: Misc.data(acheteur),
      tailleLot: parseInt(buttonAcheter.attributes['data-tailleLot']?.value ?? 1),
      quantiteIllimite: buttonAcheter.attributes['data-quantiteIllimite']?.value == 'true',
      quantiteNbLots: parseInt(buttonAcheter.attributes['data-quantiteNbLots']?.value),
      nombreLots: 1,
      prixLot: prixLot,
      prixTotal: prixLot,
      isVente: prixLot > 0
    };
    return venteData;
  }

  async onAchat() {
    await $(".nombreLots").change();
    (this.vendeur ?? this.acheteur).achatVente({
      vendeurId: this.vendeur?.id,
      acheteurId: this.acheteur?.id,
      nombreLots: this.venteData.nombreLots,
      prixTotal: this.venteData.prixTotal,
      chatMessageIdVente: this.chatMessageIdVente
    });
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    html.find(".nombreLots").change(event => this.setNombreLots(Number(event.currentTarget.value)));
  }

  setNombreLots(nombreLots) {
    this.venteData.nombreLots = nombreLots;
    this.venteData.prixTotal = (nombreLots * this.venteData.prixLot).toFixed(2);
    $(".prixTotal").text(this.venteData.prixTotal);
  }

}