import { RdDItemSort } from "./item-sort.js";
import { RdDUtility } from "./rdd-utility.js";
import { RdDAlchimie } from "./rdd-alchimie.js";
import { RdDItemCompetence } from "./item-competence.js";
import { RdDHerbes } from "./rdd-herbes.js";
import { Misc } from "./misc.js";
import { HtmlUtility } from "./html-utility.js";
import { ReglesOptionelles } from "./regles-optionelles.js";

/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class RdDItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["foundryvtt-reve-de-dragon", "sheet", "item"],
      template: "systems/foundryvtt-reve-de-dragon/templates/item-sheet.html",
      width: 550,
      height: 550
      //tabs: [{navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description"}]
    });
  }

  /* -------------------------------------------- */
  _getHeaderButtons() {
    let buttons = super._getHeaderButtons();
    const videSiConteneur = this.object.isConteneur() ? this.object.isVide() : true;
    // Add "Post to chat" button
    // We previously restricted this to GM and editable items only. If you ever find this comment because it broke something: eh, sorry!
    if ("cout" in Misc.templateData(this.object) && videSiConteneur) {
      buttons.unshift({
        class: "post",
        icon: "fas fa-comments-dollar",
        onclick: ev => this.item.proposerVente()
      });
    }
    else {
      buttons.unshift({
        class: "post",
        icon: "fas fa-comment",
        onclick: ev => this.item.postItem()
      });
    }
    return buttons
  }

  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetHeader = this.element.find(".sheet-header");
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - sheetHeader[0].clientHeight;
    sheetBody.css("height", bodyHeight);
    return position;
  }

  /* -------------------------------------------- */
  async getData() {
    const objectData = Misc.data(this.object);

    let formData = {
      title: objectData.name,
      id: objectData.id,
      type: objectData.type,
      img: objectData.img,
      name: objectData.name,
      data: objectData.data,
      isGM: game.user.isGM,
      owner: this.document.isOwner,
      editable: this.isEditable,
      cssClass: this.isEditable ? "editable" : "locked",
      isSoins: false
    }
    if (this.actor) {
      formData.isOwned = true;
      formData.actorId = this.actor.id;
    }

    formData.categorieCompetences = RdDItemCompetence.getCategorieCompetences();
    if (formData.type == 'tache' || formData.type == 'livre' || formData.type == 'meditation' || formData.type == 'oeuvre') {
      formData.caracList = duplicate(game.system.model.Actor.personnage.carac);
      formData.competences = await RdDUtility.loadCompendium('foundryvtt-reve-de-dragon.competences');
    }
    if (formData.type == 'arme') {
      formData.competences = await RdDUtility.loadCompendium('foundryvtt-reve-de-dragon.competences', it => RdDItemCompetence.isCompetenceArme(it));
      console.log(formData.competences);
    }
    if (formData.type == 'recettealchimique') {
      RdDAlchimie.processManipulation(objectData, this.actor && this.actor.id);
    }
    if (formData.type == 'potion') {
      if (this.dateUpdated) {
        formData.data.prdate = this.dateUpdated;
        this.dateUpdated = undefined;
      }
      RdDHerbes.updatePotionData(formData);
    }
    if (formData.isOwned && formData.type == 'herbe' && (formData.data.categorie == 'Soin' || formData.data.categorie == 'Repos')) {
      formData.isIngredientPotionBase = true;
    }
    formData.bonusCaseList = RdDItemSort.getBonusCaseList(formData, true);

    return formData;
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".item-cout"), ReglesOptionelles.isUsing('afficher-prix-joueurs') || game.user.isGM || !this.object.isOwned);
    HtmlUtility._showControlWhen($(".item-magique"), this.object.isMagique());
    
    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Select competence categorie
    html.find(".categorie").change(event => this._onSelectCategorie(event));

    html.find('.sheet-competence-xp').change((event) => {
      if (this.object.data.type == 'competence') {
        RdDUtility.checkThanatosXP(this.object.data.name);
      }
    });

    html.find('.enchanteDate').change((event) => {
      let jour = Number($('#jourMois').val());
      let mois = $('#nomMois').val();
      this.dateUpdated = game.system.rdd.calendrier.getIndexFromDate(jour, mois);
    });

    html.find('.creer-tache-livre').click((event) => {
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get(actorId);
      actor.creerTacheDepuisLivre(this.item);
    });
    html.find('.consommer-potion').click((event) => {
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get(actorId);
      actor.consommerPotion(this.item);
    });
    html.find('.creer-potion-base').click((event) => {
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let actor = game.actors.get(actorId);
      actor.dialogFabriquerPotion(this.item);
    });

    html.find('.alchimie-tache a').click((event) => {
      let actorId = event.currentTarget.attributes['data-actor-id'].value;
      let recetteId = event.currentTarget.attributes['data-recette-id'].value;
      let tacheName = event.currentTarget.attributes['data-alchimie-tache'].value;
      let tacheData = event.currentTarget.attributes['data-alchimie-data'].value;
      let actor = game.actors.get(actorId);
      if (actor) {
        actor.effectuerTacheAlchimie(recetteId, tacheName, tacheData);
      } else {
        ui.notifications.info("Impossible trouver un actur pour réaliser cette tache Alchimique.");
      }
    });

  }

  /* -------------------------------------------- */
  async _onSelectCategorie(event) {
    event.preventDefault();

    if (this.object.isCompetence()){
      let level = RdDItemCompetence.getNiveauBase(event.currentTarget.value);
      Misc.templateData(this.object).base = level;
      $("#base").val(level);
    }
  }

  /* -------------------------------------------- */
  get template() {
    //console.log(this);
    let type = this.object.data.type;
    return `systems/foundryvtt-reve-de-dragon/templates/item-${type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  _updateObject(event, formData) { // Deprecated en v0.8 à clarifier    
    //console.log("UPDATE !", formData);
    // Données de bonus de cases ?
    formData = RdDItemSort.buildBonusCaseStringFromFormData(formData);

    return this.object.update(formData);
  }
}
