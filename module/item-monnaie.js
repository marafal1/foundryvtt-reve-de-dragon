import { Misc } from "./misc.js";

const monnaiesData = [
  {
    name: "Etain (1 denier)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_etain_poisson.webp",
    data: { quantite: 0, valeur_deniers: 1, encombrement: 0.001, description: "" }
  },
  {
    name: "Bronze (10 deniers)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_bronze_epees.webp",
    data: { quantite: 0, valeur_deniers: 10, encombrement: 0.002, description: "" }
  },
  {
    name: "Argent (1 sol)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_argent_sol.webp",
    data: { quantite: 0, valeur_deniers: 100, encombrement: 0.003, description: "" }
  },
  {
    name: "Or (10 sols)", type: 'monnaie',
    img: "systems/foundryvtt-reve-de-dragon/icons/objets/piece_or_sol.webp",
    data: { quantite: 0, valeur_deniers: 1000, encombrement: 0.004, description: "" }
  }
]

export class Monnaie {

  static isSystemMonnaie(item) {
    let present = monnaiesData.find(monnaie => monnaie.data.valeur_deniers == Misc.data(item)?.data?.valeur_deniers);
    return present;
  }

  static monnaiesData() {
    return monnaiesData;
  }

  static filtrerMonnaies(items) {
    return items.filter(it => Misc.data(it).type == 'monnaie');
  }

  static monnaiesManquantes(items) {
    const valeurs = Monnaie.filtrerMonnaies(items)
      .map(it => Misc.templateData(it).valeur_deniers);
    const manquantes = monnaiesData.filter(monnaie => !valeurs.find(v => v != Misc.templateData(monnaie).valeur_deniers));
    //const manquantes = monnaiesData.filter(monnaie => !valeurs.find(v => v != Misc.templateData(monnaie).valeur_deniers) );
    //console.log("Valeurs : ", valeurs, manquantes);
    return []; //manquantes;
  }

  static deValeur(monnaie, v) {
    return v != monnaie.data.valeur_deniers;
  }

  static arrondiDeniers(sols) {
    return sols.toFixed(2);
  }

  static triValeurDenier() {
    return Misc.ascending(item => Misc.data(item).data.valeur_deniers);
  }
}
