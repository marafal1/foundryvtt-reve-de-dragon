import { Misc } from "./misc.js"
import { RdDDice } from "./rdd-dice.js";

const poesieHautReve = [
  {
    reference: 'Le Ratier Bretonien',
    extrait: `Le courant du Fleuve
          <br>Te domine et te Porte
          <br>Avant que tu te moeuves
          <br>Combat le, ou il t'emporte`
  },
  {
    reference: 'Incompatibilité, Charles Beaudelaire',
    extrait: `Et lorsque par hasard une nuée errante
          <br>Assombrit dans son vol le lac silencieux,
          <br>On croirait voir la robe ou l'ombre transparente
          <br>D'un esprit qui voyage et passe dans les cieux.`
  },
  {
    reference: 'Au fleuve de Loire, Joachim du Bellay',
    extrait: `Ô de qui la vive course
          <br>Prend sa bienheureuse source,
          <br>D’une argentine fontaine,
          <br>Qui d’une fuite lointaine,
          <br>Te rends au sein fluctueux
          <br>De l’Océan monstrueux`
  },
  {
    reference: 'Denis Gerfaud',
    extrait: `Et l'on peut savoir qui est le maître d'Oniros, c'est le Fleuve de l'Oubli.
            Et l'on sait qui est le créateur du Fleuve de l'Oubli, c'est Hypnos et Narcos.
            Mais l'on ne sait pas qui est le maître du Fleuve de l'Oubli,
            sinon peut-être lui-même, ou peut-être Thanatos` },
  {
    reference: 'Denis Gerfaud',
    extrait: `Narcos est la source du Fleuve de l'Oubli et Hypnos l'embouchure
            Remonter le Fleuve est la Voie de la Nuit, la Voie du Souvenir.
            Descendre le Fleuve est la Voie du Jour, la Voie de l'Oubli`
  },
  {
    reference: 'Denis Gerfaud',
    extrait: `Narcos engendre le fils dont il est la mère à l'heure du Vaisseau,
              car Oniros s'embarque pour redescendre le Fleuve
              vers son père Hypnos sur la Voie de l'Oubli`
  },
  {
    reference: 'Denis Gerfaud',
    extrait: `Hypnos engendre le fils dont il est la mère à l'heure du Serpent, car
              tel les serpents, Oniros commence à remonter le Fleuve
              sur le Voie du Souvenir vers son père Narcos`
  },
  {
    reference: 'Denis Gerfaud',
    extrait: `Ainsi se succèdent les Jours et les Ages.
          <br>Les jours des Dragons sont les Ages des Hommes.`
  },
  {
    reference: 'Denis Gerfaud',
    extrait: `Ainsi parlent les sages:
              &laquo;Les Dragons sont créateurs de leurs rêves, mais ils ne sont pas créateurs d'Oniros
              Les Dragons ne sont pas les maîtres de leurs rêvezs, car ils ne sont pas maîtres d'Oniros.
              Nul ne sait qui est le créateur des Dragons, ni qui est leur maître.
              Mais l'on peut supposer qui est le maître du Rêve des Dragons, c'est Oniros&raquo;`
  },
]

export class Poetique {
  static async getExtrait(){
    return await RdDDice.rollOneOf(poesieHautReve);
  }

}