/* -------------------------------------------- */
import { Misc } from "./misc.js";
import { TMRUtility } from "./tmr-utility.js";

/* -------------------------------------------- */
export class RdDItemSort extends Item {

  /* -------------------------------------------- */
  static isDifficulteVariable(sort) {
    return sort && (sort.data.difficulte.toLowerCase() == "variable");
  }

  /* -------------------------------------------- */
  static isCoutVariable(sort) {
    return sort && (sort.data.ptreve.toLowerCase() == "variable" || sort.data.ptreve.indexOf("+") >= 0);
  }
 
  /* -------------------------------------------- */
  static setCoutReveReel(sort){
    if (sort) {
      sort.data.ptreve_reel = this.isCoutVariable(sort) ? 1 : sort.data.ptreve;
    }
  }

  /* -------------------------------------------- */
  static getDifficulte(sort, variable) {
    if (sort && !RdDItemSort.isDifficulteVariable(sort)) {
       return Misc.toInt(sort.data.difficulte);
    }
    return variable;
  }

  /* -------------------------------------------- */
  static buildBonusCaseList( caseBonusString, newCase ) {
      if (caseBonusString == undefined) {
        return [];
      }
      let bonusCaseList = [];
      let bonusCaseArray = caseBonusString == undefined ? [] : caseBonusString.split(',');
      for( let bonusCase of bonusCaseArray) {
        let bonusSplit = bonusCase.split(':');
        bonusCaseList.push( { case: bonusSplit[0], bonus: bonusSplit[1] } );
      }
      if ( newCase )
        bonusCaseList.push( {case: "Nouvelle", bonus: 0} );
      return bonusCaseList;
    }
    
  /* -------------------------------------------- */
  /**
   * Retourne une liste de bonus/case pour un item-sheet
  * @param {} item 
  */
  static getBonusCaseList( item, newCase = false ) {
    // Gestion spéciale case bonus
    if ( item.type == 'sort') {
      return this.buildBonusCaseList(item.data.bonuscase, newCase );
    }
    return undefined;
  }
    
    /* -------------------------------------------- */
    /** Met à jour les données de formulaire 
     * si static des bonus de cases sont présents 
     * */
    static buildBonusCaseStringFromFormData( formData ) {
      if ( formData.bonusValue ) {
        let list = [];
        let caseCheck = {};
        for(let i=0; i<formData.bonusValue.length; i++) {
          let coord = formData.caseValue[i] || 'A1';
          coord = coord.toUpperCase();
          if ( TMRUtility.verifyTMRCoord( coord  ) ) { // Sanity check
            let bonus = formData.bonusValue[i] || 0;
            if ( bonus > 0 && caseCheck[coord] == undefined ) {
              caseCheck[coord] = bonus;
              list.push( coord+":"+bonus );  
            }
          }
        }
        formData.bonusValue = undefined; 
        formData.caseValue  = undefined; 
        formData['data.bonuscase'] = list.toString(); // Reset
      }
      return formData;
  }
  
  /* -------------------------------------------- */
  static incrementBonusCase( actor, sort, coord ) {
      let bonusCaseList = this.buildBonusCaseList(sort.data.bonuscase, false);
      //console.log("ITEMSORT", sort, bonusCaseList);
      
      let found = false;
      let StringList = []; 
      for( let bc of bonusCaseList) {
        if (bc.case == coord) { // Case existante
          found = true;
          bc.bonus = Number(bc.bonus) + 1;
        }
        StringList.push( bc.case+':'+bc.bonus );
      }
      if ( !found) { //Nouvelle case, bonus de 1
        StringList.push(coord+':1');
      }
  
      // Sauvegarde/update
      let bonuscase = StringList.toString();
      //console.log("Bonus cae :", bonuscase);
      actor.updateEmbeddedDocuments('Item', [{ _id: sort._id, 'data.bonuscase': bonuscase }] );
  }
  
  /* -------------------------------------------- */
  static getCaseBonus( sort, coord) {
    let bonusCaseList = this.buildBonusCaseList(sort.data.bonuscase, false);
    for( let bc of bonusCaseList) {
      if (bc.case == coord) { // Case existante
        return Number(bc.bonus);
      }
    }
    return 0;
  }

}