
/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { HtmlUtility } from "./html-utility.js";
import { RdDUtility } from "./rdd-utility.js";
import { RdDActorSheet } from "./actor-sheet.js";
import { RdDCarac } from "./rdd-carac.js";

/* -------------------------------------------- */
export class RdDActorCreatureSheet extends RdDActorSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["rdd", "sheet", "actor"],
      template: "systems/foundryvtt-reve-de-dragon/templates/actor-creature-sheet.html",
      width: 640,
      height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }]
    });
  }


  /* -------------------------------------------- */
  async getData() {
    let formData = await super.getData();
    console.log("Creature : ", formData);
    formData.calc = {
      caracTotal: RdDCarac.computeTotal(formData.data.carac),
      resumeBlessures: this.actor.computeResumeBlessure(formData.data.blessures),
      encTotal: await this.actor.computeEncombrementTotalEtMalusArmure(),
    }
    formData.calc.surEncombrementMessage = (formData.data.compteurs.surenc.value < 0) ? "Sur-Encombrement!" : "";

    RdDUtility.filterItemsPerTypeForSheet(formData);
    this.objetVersConteneur = RdDUtility.buildArbreDeConteneurs(formData.conteneurs, formData.objets);
    formData.conteneurs = RdDUtility.conteneursRacine(formData.conteneurs);

    console.log("Creature : ", this.objetVersConteneur, formData);

    return formData;
  }

  /* -------------------------------------------- */
  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // On competence change
    html.find('.creature-carac').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "carac_value", parseInt(event.target.value));
    });
    html.find('.creature-niveau').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "niveau", parseInt(event.target.value));
    });
    html.find('.creature-dommages').change((event) => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCreatureCompetence(compName, "dommages", parseInt(event.target.value));
    });
  }

  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }
}
