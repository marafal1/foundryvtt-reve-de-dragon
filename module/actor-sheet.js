/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */

import { RdDUtility } from "./rdd-utility.js";
import { HtmlUtility } from "./html-utility.js";
import { RdDItemArme } from "./item-arme.js";
import { RdDItemCompetence } from "./item-competence.js";
import { RdDBonus } from "./rdd-bonus.js";
import { Misc } from "./misc.js";
import { RdDCombatManager } from "./rdd-combat.js";
import { RdDCarac } from "./rdd-carac.js";
import { RdDItem } from "./item.js";
import { DialogSplitItem } from "./dialog-split-item.js";
import { ReglesOptionelles } from "./regles-optionelles.js";

/* -------------------------------------------- */
export class RdDActorSheet extends ActorSheet {

  /** @override */
  static get defaultOptions() {
    RdDUtility.initAfficheContenu();
    return mergeObject(super.defaultOptions, {
      classes: ["rdd", "sheet", "actor"],
      template: "systems/foundryvtt-reve-de-dragon/templates/actor-sheet.html",
      width: 640,
      //height: 720,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "carac" }],
      dragDrop: [{ dragSelector: ".item-list .item", dropSelector: null }],
      editCaracComp: false,
      showCompNiveauBase: false,
      montrerArchetype: false,
      hideControls: true
    });
  }

  /* -------------------------------------------- */
  async getData() {
    const objectData = Misc.data(this.object);
    
    //this.actor.checkMonnaiePresence(this.actor.data.items); // Always check 

    let formData = {
      title: this.title,
      id: objectData.id,
      type: objectData.type,
      img: objectData.img,
      name: objectData.name,
      editable: this.isEditable,
      cssClass: this.isEditable ? "editable" : "locked",
      data: foundry.utils.deepClone(Misc.templateData(this.object)),
      effects: this.object.effects.map(e => foundry.utils.deepClone(e.data)),
      limited: this.object.limited,
      options: this.options,
      owner: this.document.isOwner,
      itemsByType: Misc.classify(this.object.items.map(i => foundry.utils.deepClone(i.data))),
    };

    RdDUtility.filterItemsPerTypeForSheet(formData);

    formData.options.isGM = game.user.isGM;

    if (formData.type == 'creature') return formData; // Shortcut

    formData.competenceByCategory = Misc.classify(formData.competences, it => it.data.categorie);

    formData.calc = {
      comptageArchetype: RdDItemCompetence.computeResumeArchetype(formData.competences),
      competenceXPTotal: RdDItemCompetence.computeTotalXP(formData.competences),
      caracTotal: RdDCarac.computeTotal(formData.data.carac, formData.data.beaute),
      // Mise à jour de l'encombrement total et du prix de l'équipement
      encTotal: await this.actor.computeEncombrementTotalEtMalusArmure(),
      prixTotalEquipement: this.actor.computePrixTotalEquipement(),
      surprise: RdDBonus.find(this.actor.getSurprise(false)).descr,
      fatigue: RdDUtility.calculFatigueHtml(formData.data.sante.fatigue.value, formData.data.sante.endurance.max),
      resumeBlessures: this.actor.computeResumeBlessure(formData.data.blessures),
    };
    formData.calc.surEncombrementMessage = (formData.data.compteurs.surenc.value < 0) ? "Sur-Encombrement!" : "";

    formData.competences.forEach(item => {
      item.visible = !this.options.showCompNiveauBase || !RdDItemCompetence.isNiveauBase(item);
      RdDItemCompetence.levelUp(item);
    });

    Object.values(formData.data.carac).forEach(c => {
      RdDCarac.levelUp(c);
    });


    // toujours avoir une liste d'armes (pour mettre esquive et corps à corps)
    formData.combat = duplicate(formData.armes ?? []);
    RdDItemArme.computeNiveauArmes(formData.combat, formData.competences);
    RdDItemArme.ajoutCorpsACorps(formData.combat, formData.competences, formData.data.carac);
    formData.esquive = RdDItemCompetence.getEsquive(formData.competences);
    formData.combat = RdDCombatManager.finalizeArmeList(formData.combat, formData.competences, formData.data.carac);

    this.armesList = formData.combat;

    // Common data
    formData.ajustementsConditions = CONFIG.RDD.ajustementsConditions;
    formData.difficultesLibres = CONFIG.RDD.difficultesLibres;

    formData.hautreve = {
      isDemiReve: this.actor.listeEffets( it => it.label == "Demi-rêve").length > 0,
      sortsReserve: formData.data.reve.reserve.list,
      rencontres: duplicate(formData.data.reve.rencontre.list),
      casesTmr: formData.itemsByType.casetmr,
      cacheTMR: this.actor.isTMRCache()
    }

    this.objetVersConteneur = RdDUtility.buildArbreDeConteneurs(formData.conteneurs, formData.objets);
    formData.conteneurs = RdDUtility.conteneursRacine(formData.conteneurs);

    formData.subacteurs = {
      vehicules: this.actor.listeVehicules(),
      montures: this.actor.listeMontures(),
      suivants: this.actor.listeSuivants()
    }
    if (this.actor.getBestDraconic().data.niveau > -11 && !this.actor.isHautRevant()) {
      ui.notifications.error(`${this.actor.name} a des compétences draconiques, mais pas le don de Haut-Rêve!
        <br>Ajoutez-lui la tête "Don de Haut-Rêve" pour lui permettre d'utiliser ses compétences et d'accéder aux terres médianes du rêve`);
    }
    return formData;
  }

  isCompetenceAffichable(competence) {
    return !this.options.showCompNiveauBase || !RdDItemCompetence.isNiveauBase(competence);
  }

  /* -------------------------------------------- */
  async _onDropActor(event, dragData) {
    console.log("DRAG", this.actor.id, dragData);
    this.actor.addSubacteur(dragData.id || dragData.data._id);
    super._onDropActor(event, dragData);
  }

  /* -------------------------------------------- */
  async _onDropItem(event, dragData) {
    const callSuper = await this.actor.processDropItem(event, dragData, this.objetVersConteneur);
    if (callSuper) {
      await super._onDropItem(event, dragData)
    }
  }

  /* -------------------------------------------- */
  async createItem(name, type) {
    await this.actor.createEmbeddedDocuments('Item', [{ name: name, type: type }], { renderSheet: true });
  }

  /* -------------------------------------------- */
  async createEmptyTache() {
    await this.createItem('Nouvelle tache', 'tache');
  }

  /* -------------------------------------------- */  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    HtmlUtility._showControlWhen($(".gm-only"), game.user.isGM);
    HtmlUtility._showControlWhen($(".appliquerFatigue"), ReglesOptionelles.isUsing("appliquer-fatigue"));

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    html.find('.item-split').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("item-id"));
      this.splitItem(item);
    });
    html.find('.item-edit').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      const item = this.actor.items.get(li.data("item-id"));
      item.sheet.render(true);
    });
    html.find('.display-label a').click(async event => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      const item = this.actor.getEmbeddedDocument('Item', myID);
      item.sheet.render(true);
    });
    html.find('.rencontre-delete').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      const rencontreKey = li.data("item-id");
      this.actor.deleteTMRRencontre(rencontreKey);
    });
    html.find('.item-delete').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      RdDUtility.confirmerSuppression(this, li);
    });
    html.find('.item-vendre').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      const itemId = li.data("item-id");
      const item = this.actor.getObjet(itemId);
      item?.proposerVente();
    });
    html.find('.item-action').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      const itemId = li.data("item-id");
      const item = this.actor.getObjet(itemId);
      this.actor.actionItem(item);
    });
    html.find('.subacteur-delete').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      RdDUtility.confirmerSuppressionSubacteur(this, li);
    });

    html.find('.encaisser-direct').click(async event => {
      this.actor.encaisser();
    });

    html.find('.remise-a-neuf').click(async event => {
      if (game.user.isGM) {
        this.actor.remiseANeuf();
      }
    });
    html.find('.creer-tache').click(async event => {
      this.createEmptyTache();
    });
    html.find('.creer-un-objet').click(async event => {
      RdDUtility.selectObjetType( this );
    });
    html.find('.creer-une-oeuvre').click(async event => {
      RdDUtility.selectTypeOeuvre(this);
    });
    html.find('#nettoyer-conteneurs').click(async event => {
      this.actor.nettoyerConteneurs();
    });

    // Blessure control
    html.find('.blessure-control').click(async event => {
      const tr = $(event.currentTarget).parents(".item");
      let btype = tr.data("blessure-type");
      let index = tr.data('blessure-index');
      let active = $(event.currentTarget).data('blessure-active');
      //console.log(btype, index, active);
      await this.actor.manageBlessureFromSheet(btype, index, active);
    });

    // Blessure data
    html.find('.blessures-soins').change(async event => {
      const tr = $(event.currentTarget).parents(".item");
      let btype = tr.data('blessure-type');
      let index = tr.data('blessure-index');
      let psoins = tr.find('.blessure-premiers_soins').val();
      let pcomplets = tr.find('.blessure-soins_complets').val();
      let jours = tr.find('.blessure-jours').val();
      let loc = tr.find('.blessure-localisation').val();
      //console.log(btype, index, psoins, pcomplets, jours, loc);
      await this.actor.setDataBlessureFromSheet(btype, index, psoins, pcomplets, jours, loc);
    });

    // Equip Inventory Item
    html.find('.item-equip').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.equiperObjet(li.data("item-id"));
    });

    // Roll Carac
    html.find('.carac-label a').click(async event => {
      let caracName = event.currentTarget.attributes.name.value;
      this.actor.rollCarac(caracName.toLowerCase());
    });

    html.find('.chance-actuelle').click(async event => {
      this.actor.rollCarac('chance-actuelle');
    });

    html.find('.chance-appel').click(async event => {
      this.actor.rollAppelChance();
    });

    html.find('#jet-astrologie').click(async event => {
      this.actor.astrologieNombresAstraux();
    });

    // Roll Skill
    html.find('a.competence-label').click(async event => {
      let compName = event.currentTarget.name;
      this.actor.rollCompetence(compName);
    });
    html.find('.tache-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let tacheId = li.data('item-id');
      this.actor.rollTache(tacheId);
    });
    html.find('.meditation-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let meditationId = li.data('item-id');
      this.actor.rollMeditation(meditationId);
    });
    html.find('.chant-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let chantId = li.data('item-id');
      this.actor.rollChant(chantId);
    });
    html.find('.danse-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let danseId = li.data('item-id');
      this.actor.rollDanse(danseId);
    });
    html.find('.musique-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let musiqueId = li.data('item-id');
      this.actor.rollMusique(musiqueId);
    });
    html.find('.oeuvre-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let oeuvreId = li.data('item-id');
      this.actor.rollOeuvre(oeuvreId);
    });
    html.find('.jeu-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let jeuId = li.data('item-id');
      this.actor.rollJeu(jeuId);
    });
    html.find('.recettecuisine-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let recetteId = li.data('item-id');
      this.actor.rollRecetteCuisine(recetteId);
    });
    html.find('.subacteur-label a').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      let actorId = li.data('actor-id');
      let actor = game.actors.get(actorId);
      if (actor) {
        actor.sheet.render(true);
      }
    });
    
    // Boutons spéciaux MJs
    html.find('.forcer-tmr-aleatoire').click(async event => {
      this.actor.cacheTMRetMessage();
    });
    html.find('.afficher-tmr').click(async event => {
      this.actor.afficheTMRetMessage();
    });
    
    // Points de reve actuel
    html.find('.ptreve-actuel a').click(async event => {
      this.actor.rollCarac('reve-actuel');
    });

    // Roll Weapon1
    html.find('.arme-label a').click(async event => {
      let armeName = event.currentTarget.text;
      let competenceName = event.currentTarget.attributes['data-competence-name'].value;
      this.actor.rollArme(competenceName, armeName);
    });
    // Initiative pour l'arme
    html.find('.arme-initiative a').click(async event => {
      let combatant = game.combat.data.combatants.find(c => c.actor.data._id == this.actor.data._id);
      if (combatant) {
        let armeName = event.currentTarget.attributes['data-arme-name'].value;
        let arme = this.armesList.find(a => a.name == armeName);
        RdDCombatManager.rollInitiativeCompetence(combatant._id, arme);
      } else {
        ui.notifications.info("Impossible de lancer l'initiative sans être dans un combat.");
      }
    });
    // Display TMR, visuualisation
    html.find('.visu-tmr').click(async event => {
      this.actor.displayTMR("visu");
    });

    // Display TMR, normal
    html.find('.monte-tmr').click(async event => {
      this.actor.displayTMR("normal");
    });

    // Display TMR, fast 
    html.find('.monte-tmr-rapide').click(async event => {
      this.actor.displayTMR("rapide");
    });

    html.find('.dormir-une-heure').click(async event => {
      this.actor.dormir(1);
    });
    html.find('.dormir-chateau-dormant').click(async event => {
      this.actor.dormirChateauDormant();
    });
    html.find('.enlever-tous-effets').click(async event => {
      this.actor.enleverTousLesEffets();
    });
    html.find('.conteneur-name a').click(async event => {
      let myID = event.currentTarget.attributes['data-item-id'].value;
      RdDUtility.toggleAfficheContenu(myID);
      this.render(true);
    });
    html.find('.carac-xp-augmenter').click(async event => {
      let caracName = event.currentTarget.name.replace("augmenter.", "");
      this.actor.updateCaracXPAuto(caracName);
    });
    html.find('.competence-xp-augmenter').click(async event => {
      let compName = event.currentTarget.attributes.compname.value;
      this.actor.updateCompetenceXPAuto(compName);
    });
    
    if (this.options.editCaracComp) {
      // On carac change
      html.find('.carac-value').change(async event => {
        let caracName = event.currentTarget.name.replace(".value", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCarac(caracName, parseInt(event.target.value));
      });
      html.find('.carac-xp').change(async event => {
        let caracName = event.currentTarget.name.replace(".xp", "").replace("data.carac.", "");
        //console.log("Value changed :", event, caracName);
        this.actor.updateCaracXP(caracName, parseInt(event.target.value));
      });
      // On competence change
      html.find('.competence-value').change(async event => {
        let compName = event.currentTarget.attributes.compname.value;
        //console.log("Competence changed :", compName);
        this.actor.updateCompetence(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp').change(async event => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXP(compName, parseInt(event.target.value));
      });
      // On competence xp change
      html.find('.competence-xp-sort').change(async event => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceXPSort(compName, parseInt(event.target.value));
      });
      // On competence archetype change
      html.find('.competence-archetype').change(async event => {
        let compName = event.currentTarget.attributes.compname.value;
        this.actor.updateCompetenceArchetype(compName, parseInt(event.target.value));
      });
    }
    
    html.find('#show-hide-competences').click(async event => {
      this.options.showCompNiveauBase = !this.options.showCompNiveauBase;
      this.render(true);
    });
    html.find('.lock-unlock-sheet').click(async event => {
      this.options.editCaracComp = !this.options.editCaracComp;
      this.render(true);
    });
    html.find('#show-hide-archetype').click(async event => {
      this.options.montrerArchetype = !this.options.montrerArchetype;
      this.render(true);
    });
    html.find('.lock-unlock-controls').click(async event => {
      this.options.hideControls = !this.options.hideControls;
      this.render(true);
    });

    // On pts de reve change
    html.find('.pointsreve-value').change(async event => {
      let reveValue = event.currentTarget.value;
      this.actor.update({ "data.reve.reve.value": reveValue });
    });

    // On seuil de reve change
    html.find('.seuil-reve-value').change(async event => {
      console.log("seuil-reve-value", event.currentTarget)
      this.actor.setPointsDeSeuil(event.currentTarget.value);
    });

    html.find('#attribut-protection-edit').change(async event => {
      this.actor.updateAttributeValue(event.currentTarget.attributes.name.value, parseInt(event.target.value));
    });

    // On stress change
    html.find('.compteur-edit').change(async event => {
      let fieldName = event.currentTarget.attributes.name.value;
      this.actor.updateCompteurValue(fieldName, parseInt(event.target.value));
    });

    html.find('#ethylisme').change(async event => {
      this.actor.setEthylisme(parseInt(event.target.value));
    });
    html.find('.stress-test').click(async event => {
      this.actor.transformerStress();
    });
    html.find('.moral-malheureux').click(async event => {
      this.actor.jetDeMoral('malheureuse');
    });
    html.find('.moral-neutre').click(async event => {
      this.actor.jetDeMoral('neutre');
    });
    html.find('.moral-heureux').click(async event => {
      this.actor.jetDeMoral('heureuse');
    });
    html.find('#ethylisme-test').click(async event => {
      this.actor.jetEthylisme();
    });

    html.find('#jet-vie').click(async event => {
      this.actor.jetVie();
    });
    html.find('#jet-endurance').click(async event => {
      this.actor.jetEndurance();
    });

    html.find('.monnaie-plus').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), 1);
    });
    html.find('.monnaie-moins').click(async event => {
      const li = $(event.currentTarget).parents(".item");
      this.actor.monnaieIncDec(li.data("item-id"), -1);
    });

    html.find('#vie-plus').click(async event => {
      this.actor.santeIncDec("vie", 1);
    });
    html.find('#vie-moins').click(async event => {
      this.actor.santeIncDec("vie", -1);
    });
    html.find('#endurance-plus').click(async event => {
      this.actor.santeIncDec("endurance", 1);
    });
    html.find('#endurance-moins').click(async event => {
      this.actor.santeIncDec("endurance", -1);
    });
    html.find('.data-sante-sonne').click(async event => {
      this.actor.setSonne(event.currentTarget.checked);
    });
    html.find('#ptreve-actuel-plus').click(async event => {
      this.actor.reveActuelIncDec(1);
    });
    html.find('#ptreve-actuel-moins').click(async event => {
      this.actor.reveActuelIncDec(-1);
    });
    html.find('#fatigue-plus').click(async event => {
      this.actor.santeIncDec("fatigue", 1);
    });
    html.find('#fatigue-moins').click(async event => {
      this.actor.santeIncDec("fatigue", -1);
    });
  }


  /* -------------------------------------------- */
  /** @override */
  setPosition(options = {}) {
    const position = super.setPosition(options);
    const sheetHeader = this.element.find(".sheet-header");
    const sheetTabs = this.element.find(".sheet-tabs");
    const sheetBody = this.element.find(".sheet-body");
    const bodyHeight = position.height - sheetHeader[0].clientHeight - sheetTabs[0].clientHeight;
    sheetBody.css("height", bodyHeight);
    return position;
  }


  /* -------------------------------------------- */
  /** @override */
  _updateObject(event, formData) {
    // Update the Actor
    return this.object.update(formData);
  }

  async splitItem(item) {
    const dialog = await DialogSplitItem.create(item, (item, split) => this._onSplitItem(item, split));
    dialog.render(true);
  }

  async _onSplitItem(item, split) {
    if (split >= 1 && split < Misc.data(item).data.quantite) {
      await item.diminuerQuantite(split);
      const itemData = duplicate( Misc.data(item));
      itemData.data.quantite = split;
      await this.actor.createEmbeddedDocuments('Item', [itemData])
    }
  }

}
