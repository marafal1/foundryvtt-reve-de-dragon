import { Grammar } from "./grammar.js";
import { Misc } from "./misc.js";
import { RdDUtility } from "./rdd-utility.js";

export class DialogFabriquerPotion extends Dialog {

  /* -------------------------------------------- */
  static async create(actor, item, dialogConfig) {
    let potionData = DialogFabriquerPotion.prepareData(actor, item);

    let conf = {
      title: `Fabriquer une potion de ${potionData.data.categorie}`,
      content: await renderTemplate(dialogConfig.html, potionData),
      default: potionData.buttonName,
    };

    let options = { classes: ["dialogfabriquerpotion"], width: 600, height: 160, 'z-index': 99999 };
    mergeObject(options, dialogConfig.options ?? {}, { overwrite: true })

    const dialog = new DialogFabriquerPotion(actor, potionData, conf, options);
    dialog.render(true);
    return dialog;
  }

  /* -------------------------------------------- */
  static prepareData(actor, item) {
    let potionData = duplicate(Misc.data(item));
    potionData.nbBrinsSelect = RdDUtility.buildListOptions(1, potionData.data.quantite);
    potionData.nbBrins = Math.min(potionData.data.quantite, DialogFabriquerPotion.getNombreBrinOptimal(potionData));
    potionData.buttonName = "Fabriquer";
    return potionData;
  }

  /* -------------------------------------------- */
  constructor(actor, potionData, conf, options) {
    conf.buttons = {
      [potionData.buttonName]: {
        label: potionData.buttonName, callback: it => this.onFabriquer(it)
      }
    };

    super(conf, options);

    this.actor = actor;
    this.potionData = potionData;
  }

  static getNombreBrinOptimal(herbeData) {
    switch (herbeData.data.categorie ?? '') {
      case "Soin": return 12 - herbeData.data.niveau;
      case "Repos": return 7 - herbeData.data.niveau;
    }
    return 1;
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    html.find("#nbBrins").change(event => {
      this.potionData.nbBrins = Misc.toInt(event.currentTarget.value);
    });
  }

  /* -------------------------------------------- */
  async onFabriquer(it) {
    await $("#nbBrins").change();
    this.actor.fabriquerPotion(this.potionData);
    this.close();
  }
}