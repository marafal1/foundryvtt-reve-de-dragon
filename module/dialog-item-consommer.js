import { Misc } from "./misc.js";

export class DialogConsommer extends Dialog {

  static async create(actor, item, template = undefined, options = {}) {
    const consommerData = DialogConsommer.prepareData(actor, item, options);
    const html = await renderTemplate(template ?? `systems/foundryvtt-reve-de-dragon/templates/dialog-item-consommer.html`, consommerData);
    return new DialogConsommer(actor, item, consommerData, html, options)
  }

  constructor(actor, item, consommerData, html, options = {}) {
    mergeObject(options, { classes: ["dialogconsommer"], width: 350, height: 450, 'z-index': 99999 }, { overwrite: false })

    let conf = {
      title: consommerData.title,
      content: html,
      default: consommerData.buttonName,
      buttons: {
        [consommerData.buttonName]: {
          label: consommerData.buttonName, callback: it => this.onConsommer(it)
        }
      }
    };

    super(conf, options);

    this.actor = actor;
    this.item = item;
    this.consommerData = consommerData;
  }

  async onConsommer(event) {
    await $(".se-forcer").change();
    await $(".consommer-doses").change();
    this.actor.consommer(this.item, this.consommerData.choix);
  }

  /* -------------------------------------------- */
  static prepareData(actor, item, options) {
    const itemData = duplicate(Misc.data(item));
    let consommerData = {
      item: itemData,
      cuisine: Misc.data(actor.getCompetence('cuisine')),
      choix: {
        doses: options.doses ?? 1,
        seForcer: options.seForcer ?? false,
      }
    }
    switch (itemData.type) {
      case 'nourritureboisson':
        consommerData.title = itemData.data.boisson ? `${itemData.name}: boire une dose` : `${itemData.name}: manger une portion`;
        consommerData.buttonName = itemData.data.boisson ? "Boire" : "Manger";
        break;
      case 'potion':
        consommerData.title = `${itemData.name}: boire la potion`;
        consommerData.buttonName = "Boire";
        break;
    }
    DialogConsommer.calculDoses(consommerData, consommerData.choix.doses)
    return consommerData;
  }

  static calculDoses(consommerData) {
    const doses = consommerData.choix.doses;
    consommerData.totalSust = Misc.keepDecimals(doses * (consommerData.item.data.sust ?? 0), 2);
    consommerData.totalDesaltere = consommerData.item.data.boisson
      ? Misc.keepDecimals(doses * (consommerData.item.data.desaltere ?? 0), 2)
      : 0;
  }


  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);
    html.find(".se-forcer").change(event => this.setSeForcer(event));
    html.find(".consommer-doses").change(event => this.selectDoses(event));
  }


  setSeForcer(event) {
    this.consommerData.choix.seForcer = event.currentTarget.checked;
  }

  selectDoses(event) {
    this.consommerData.choix.doses = Number(event.currentTarget.value);
    DialogConsommer.calculDoses(this.consommerData);
    $(".total-sust").text(this.consommerData.totalSust);
    $(".total-desaltere").text(this.consommerData.totalDesaltere);
  }
}