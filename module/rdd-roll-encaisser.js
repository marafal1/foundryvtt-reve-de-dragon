/**
 * Extend the base Dialog entity by defining a custom window to perform roll.
 * @extends {Dialog}
 */
export class RdDEncaisser extends Dialog {

  /* -------------------------------------------- */
  constructor(html, actor) {
    // Common conf
    const buttonsCreatures = {
      "mortel": { label: "mortel", callback: html => this.performEncaisser("mortel") },
      "non-mortel": { label: "non-mortel", callback: html => this.performEncaisser("non-mortel") },
    };
    const buttonsEntitesCauchemar = {
      "cauchemar": { label: "cauchemar", callback: html => this.performEncaisser("cauchemar") }
    };
    const buttons = actor.isEntiteCauchemar() ? buttonsEntitesCauchemar : buttonsCreatures;

    let dialogConf = {
      title: "Jet d'Encaissement",
      content: html,
      buttons: buttons,
      default: "coupMortel"
    }


    let dialogOptions = {
      classes: ["rdddialog"],
      width: 320,
      height: 240
    }

    // Select proper roll dialog template and stuff
    super(dialogConf, dialogOptions);

    this.actor = actor;
    this.modifier = 0;
    this.encaisserSpecial = "aucun";
  }



  /* -------------------------------------------- */
  performEncaisser(mortalite) {
    this.actor.encaisserDommages({
      dmg: {
        total: Number(this.modifier),
        encaisserSpecial: this.encaisserSpecial,
        loc: { result: 0, label: "" },
        mortalite: mortalite
      }
    });
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    // Setup everything onload
    $(function () {
      $("#modificateurDegats").val("0");
    });

    html.find('#modificateurDegats').change((event) => {
      this.modifier = event.currentTarget.value; // Update the selected bonus/malus
    });
    html.find('#encaisserSpecial').change((event) => {
      this.encaisserSpecial = event.currentTarget.value; // Update the selected bonus/malus
    });
  }

}
