import { Misc } from "./misc.js";

/**
 * Extend the base Dialog entity by defining a custom window to perform roll.
 * @extends {Dialog}
 */
export class RdDCalendrierEditeur extends Dialog {

  /* -------------------------------------------- */
  constructor(html, calendrier, calendrierData) {

    let myButtons = {
        saveButton: { label: "Enregistrer", callback: html => this.fillData() }
      };

    // Common conf
    let dialogConf = { content: html, title: "Editeur de date/heure", buttons: myButtons, default: "saveButton" };
    let dialogOptions = { classes: ["rdddialog"], width: 400, height: 300, 'z-index': 99999 }  
    super(dialogConf, dialogOptions)
    
    this.calendrier = calendrier;
    this.calendrierData = calendrierData; //duplicate(calendrierData);
  }

  /* -------------------------------------------- */
  fillData( ) {
    this.calendrierData.moisKey  = $("#nomMois").val();
    this.calendrierData.heureKey = $("#nomHeure").val();
    this.calendrierData.jourMois = $("#jourMois").val();
    this.calendrierData.minutesRelative = $("#minutesRelative").val();
    
    console.log("UPDATE  ", this.calendrierData);
    this.calendrier.saveEditeur( this.calendrierData )
  }

  /* -------------------------------------------- */
  updateData( calendrierData ) {
    this.calendrierData = duplicate(calendrierData);
  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);
    
    let calendrierData = this.calendrierData;

    $(function () {
      console.log(calendrierData);
      $("#nomMois").val(calendrierData.moisKey);
      $("#nomHeure").val(calendrierData.heureKey);
      $("#jourMois").val(calendrierData.jourMois);
      $("#minutesRelative").val(calendrierData.minutesRelative);
    });

  }

}
