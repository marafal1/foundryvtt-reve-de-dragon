import { RdDItemCompetence } from "./item-competence.js";
import { Misc } from "./misc.js";


/**
 * Extend the base Dialog entity by defining a custom window to perform roll.
 * @extends {Dialog}
 */
export class RdDAstrologieJoueur extends Dialog {

  /* -------------------------------------------- */
  static async create(actor, dialogConfig) {

    let data = {
      nombres: this.organizeNombres(actor),
      dates: game.system.rdd.calendrier.getJoursSuivants(10),
      etat: actor.getEtatGeneral(),
      ajustementsConditions: CONFIG.RDD.ajustementsConditions,
      astrologie: RdDItemCompetence.findCompetence(actor.data.items, 'Astrologie')
    }
    const html = await renderTemplate('systems/foundryvtt-reve-de-dragon/templates/dialog-astrologie-joueur.html', data);
    let options = { classes: ["rdddialog"], width: 600, height: 500, 'z-index': 99999 };
    if (dialogConfig.options) {
      mergeObject(options, dialogConfig.options, { overwrite: true });
    }
    return new RdDAstrologieJoueur(html, actor, data);
  }

  /* -------------------------------------------- */
  constructor(html, actor, data) {

    let myButtons = {
      saveButton: { label: "Fermer", callback: html => this.quitDialog() }
    };

    // Get all n
    // Common conf
    let dialogConf = { content: html, title: "Nombres Astraux", buttons: myButtons, default: "saveButton" };
    let dialogOptions = { classes: ["rdddialog"], width: 600, height: 300, 'z-index': 99999 };
    super(dialogConf, dialogOptions);

    this.actor = actor;
    this.dataNombreAstral = duplicate(data);
  }

  /* -------------------------------------------- */
  static organizeNombres(actor) {
    let itemNombres = actor.listItemsData('nombreastral');
    let itemFiltered = {};
    for (let item of itemNombres) {
      if (itemFiltered[item.data.jourindex]) {
        itemFiltered[item.data.jourindex].listValues.push(item.data.value);
      } else {
        itemFiltered[item.data.jourindex] = {
          listValues: [item.data.value],
          jourlabel: item.data.jourlabel
        }
      }
    }
    return itemFiltered;
  }

  /* -------------------------------------------- */
  requestJetAstrologie() {
    let data = {
      id: this.actor.data._id,
      carac_vue: Misc.data(this.actor).data.carac['vue'].value,
      etat: this.dataNombreAstral.etat,
      astrologie: this.dataNombreAstral.astrologie,
      conditions: $("#diffConditions").val(),
      date: $("#joursAstrologie").val()
    }
    if (game.user.isGM) {
      game.system.rdd.calendrier.requestNombreAstral(data);
    } else {
      game.socket.emit("system.foundryvtt-reve-de-dragon", {
        msg: "msg_request_nombre_astral",
        data: data
      });
    }
    this.close();
  }

  /* -------------------------------------------- */
  quitDialog() {

  }

  /* -------------------------------------------- */
  activateListeners(html) {
    super.activateListeners(html);

    $(function () {
      $("#diffConditions").val(0);
    });

    html.find('#jet-astrologie').click((event) => {
      this.requestJetAstrologie();
    });
  }

}
