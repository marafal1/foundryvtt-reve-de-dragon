import { ChatUtility } from "./chat-utility.js";
import { SYSTEM_RDD } from "./constants.js";
import { Misc } from "./misc.js";

function img(src) {
  return `<img src="${src}" class="dice-img" />`
}

function iconHeure(heure) {
  if (heure < 10) {
    heure = '0' + heure;
  }
  return `systems/foundryvtt-reve-de-dragon/icons/heures/hd${heure}.webp`
}
const imagesHeures = [1, 2, 3, 4, 5, 6, 7, 9, 9, 10, 11, 12].map(it => iconHeure(it));

const imgSigneDragon = img(imagesHeures[4]);

/** De7 pour les jets de rencontre */
export class De7 extends Die {
  /** @override */
  static DENOMINATION = "7";

  static diceSoNiceData(system) {
    return {
      type: "d7",
      font: "HeuresDraconiques",
      fontScale: 0.7,
      labels: ['1', '2', '3', '4', '5', '6', 'd', '0'],
      system: system
    }
  }

  constructor(termData) {
    termData.faces = 8;
    super(termData);
  }

  async evaluate() {
    super.evaluate();
    this.explode("x=8");
    return this;
  }

  get total() {
    return this.values.filter(it => it != 8).reduce(Misc.sum(), 0);
  }

  getResultLabel(diceTerm) {
    switch (diceTerm.result) {
      case 7: return imgSigneDragon;
    }
    return diceTerm.result.toString();
  }
}

/** DeDraconique pour le D8 sans limite avec 8=>0 */
export class DeDraconique extends Die {
  static DENOMINATION = "r";

  static diceSoNiceData(system) {
    return {
      type: "dr",
      font: "HeuresDraconiques",
      fontScale: 0.7,
      labels: ['1', '2', '3', '4', '5', '6', 'd', '0'],
      system: system
    }
  }

  constructor(termData) {
    termData.faces = 8;
    super(termData);
  }

  async evaluate() {
    super.evaluate();
    this.explode("x=7");
    return this;
  }

  get total() {
    return this.values.filter(it => it != 8).reduce(Misc.sum(), 0);
  }

  getResultLabel(diceTerm) {
    switch (diceTerm.result) {
      case 7: return imgSigneDragon;
      case 8: return '0';
    }
    return diceTerm.result.toString();
  }
}

/** De 12 avec les heures */
export class DeHeure extends Die {

  /** @override */
  static DENOMINATION = "h";

  static diceSoNiceData(system) {
    return {
      type: "dh",
      font: "HeuresDraconiques",
      labels: ['v', 'i', 'f', 'o', 'd', 'e', 'l', 's', 'p', 'a', 'r', 'c'],
      system: system
    }
  }

  constructor(termData) {
    termData.faces = 12;
    super(termData);
  }

  getResultLabel(diceTerm) {
    return img(imagesHeures[diceTerm.result - 1]);
  }
}

export class RdDDice {
  static init() {
    CONFIG.Dice.terms[De7.DENOMINATION] = De7;
    CONFIG.Dice.terms[DeDraconique.DENOMINATION] = DeDraconique;
    CONFIG.Dice.terms[DeHeure.DENOMINATION] = DeHeure;
  }
  static onReady() {
    if (game.modules.get("dice-so-nice")?.active) {
      if (game.settings.get("core", "noCanvas")) {
        ui.notifications.warn("Dice So Nice! n'affichera pas de dés car vous avez coché l'option de Foundry 'Scène de jeu désactivé' 'Disable Game Canvas' ");
      }
    }
  }

  static async roll(formula, options = { showDice: false, rollMode: undefined }) {
    const roll = new Roll(formula);
    await roll.evaluate({ async: true });
    if (!options.hideDice) {
      roll.showDice = options.showDice;
      await RdDDice.show(roll, options.rollMode ?? game.settings.get("core", "rollMode"));
    }
    return roll;
  }

  static async rollTotal(formula, options = { showDice: false, hideDice: false }) {
    const roll = await RdDDice.roll(formula, options);
    return roll.total;
  }

  static async rollOneOf(array) {
    const roll = await RdDDice.rollTotal(`1d${array.length}`);
    return array[roll - 1];
  }

  static diceSoNiceReady(dice3d) {
    for (const system of Object.keys(dice3d.DiceFactory.systems)) {
      dice3d.addDicePreset(De7.diceSoNiceData(system));
      dice3d.addDicePreset(DeDraconique.diceSoNiceData(system));
      dice3d.addDicePreset(DeHeure.diceSoNiceData(system));
    }
  }

  /* -------------------------------------------- */
  static async show(roll, rollMode) {
    if (roll.showDice || game.settings.get(SYSTEM_RDD, "dice-so-nice") == true) {
      await this.showDiceSoNice(roll, rollMode);
    }
    return roll;
  }

  /* -------------------------------------------- */
  static async showDiceSoNice(roll, rollMode) {
    if (game.modules.get("dice-so-nice")?.active) {
      if (game.dice3d) {
        let whisper = null;
        let blind = false;
        rollMode = rollMode ?? game.settings.get("core", "rollMode");
        switch (rollMode) {
          case "blindroll": //GM only
            blind = true;
          case "gmroll": //GM + rolling player
            whisper = ChatUtility.getUsers(user => user.isGM);
            break;
          case "roll": //everybody
            whisper = ChatUtility.getUsers(user => user.active);
            break;
          case "selfroll":
            whisper = [game.user.id];
            break;
        }
        await game.dice3d.showForRoll(roll, game.user, true, whisper, blind);
      }
    }
  }
}