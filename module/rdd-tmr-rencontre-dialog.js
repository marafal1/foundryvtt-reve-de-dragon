/* -------------------------------------------- */
export class RdDTMRRencontreDialog extends Dialog {

  /* -------------------------------------------- */
  constructor(html, tmrApp, rencontre, postRencontre) {
    const dialogConf = {
      title: "Rencontre en TMR!",
      content: "Vous recontrez un " + rencontre.name + " de force " + rencontre.force + "<br>",
      buttons: {
        derober: { icon: '<i class="fas fa-check"></i>', label: "Se dérober", callback: () => { this.onButtonFuir(() => tmrApp.derober()); } },
        refouler: { icon: '<i class="fas fa-check"></i>', label: "Refouler", callback: () => this.onButtonAction(() => tmrApp.refouler()) },
        maitiser: { icon: '<i class="fas fa-check"></i>', label: "Maîtriser", callback: () => this.onButtonAction(() => tmrApp.maitriserRencontre()) }
      },
      default: "derober"
    };
    if (rencontre.ignorer) {
      dialogConf.buttons.ignorer = { icon: '<i class="fas fa-check"></i>', label: "Ignorer", callback: () => this.onButtonAction(() => tmrApp.ignorerRencontre()) }
    };

    const dialogOptions = {
      classes: ["tmrrencdialog"],
      width: 320, height: 240,
      'z-index': 50
    }
    super(dialogConf, dialogOptions);

    this.toClose = false;
    this.rencontreData = duplicate(rencontre);
    this.postRencontre = postRencontre;
    this.tmrApp = tmrApp;
    this.tmrApp.minimize();
  }

  async onButtonAction(action) {
    this.toClose = true;
    await action();
    this.postRencontre();
  }
  
  async onButtonFuir(action) {
    this.toClose = true;
    await action();
  }

  /* -------------------------------------------- */
  close() {
    if (this.toClose) {
      this.tmrApp.maximize();
      return super.close();
    }
    ui.notifications.info("Vous devez résoudre la rencontre.");
  }

}
